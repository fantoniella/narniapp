import { Injectable } from '@angular/core';

import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HttpDataProvider {
  private basePath: string;
  private _isLogged: boolean;
  constructor(private _http: Http) {
    // this.basePath = 'http://besussurri.bitcodelab.com';
    this.basePath = 'http://213.32.67.54:8080';
    this._isLogged = false;
  }

  login(username: string, password: string) {
    var body = `{"username":"${username}","password":"${password}"}`;
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.post(this.basePath + '/login', body, options).toPromise()
      .then(c => { console.log(c); this._isLogged = true; return c.json().user_id })
      .catch(e => { console.log(e); this._isLogged = false; });
  };
  logout() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.get(this.basePath + '/logout', options).toPromise()
      .then(c => { console.log(c); this._isLogged = false; })
      .catch(e => { console.log(e); this._isLogged = false; });
  };
  isLogged(): boolean {
    return this._isLogged;
  };

  setLogged(status: boolean) {
    this._isLogged = status;
  }

  //CRUD
  create(tableName: string, record: object) {
    var body = JSON.stringify(record);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.post(this.basePath + '/' + tableName, body, options).toPromise()
  };
  readAll(tableName: string) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.get(this.basePath + '/' + tableName, options).toPromise()
  };
  readSingle(tableName: string, id: number) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.get(this.basePath + '/' + tableName + '/' + id, options).toPromise()
  };
  readSpecial(pathName: string, value: string) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.get(this.basePath + '/' + pathName + '/' + value, options).toPromise()
  };
  update(tableName: string, id: number, record: object) {
    var body = JSON.stringify(record);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.put(this.basePath + '/' + tableName + '/' + id, body, options).toPromise()
  };
  delete(tableName: string, id: number) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.delete(this.basePath + '/' + tableName + '/' + id, options).toPromise()
  };

  //MANY-TO-MANY
  attach(tableName: string, id: number, tableName2: string, id2: number) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.get(this.basePath + '/' + tableName + '/' + id + '/attach/' + tableName2 + '/' + id2, options).toPromise()
  }
  detach(tableName: string, id: number, tableName2: string, id2: number) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this._http.get(this.basePath + '/' + tableName + '/' + id + '/detach/' + tableName2 + '/' + id2, options).toPromise()
  }
}
