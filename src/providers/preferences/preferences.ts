import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the PreferencesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PreferencesProvider {
  public lang: String;
  constructor(public http: HttpClient) {
    console.log('Hello PreferencesProvider Provider');
    this.lang='it';
  }
  setLang(lang: String){
    this.lang = lang;
    console.log("Lang changed to "+lang)
  }

}
