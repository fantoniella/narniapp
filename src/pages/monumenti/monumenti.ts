import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpDataProvider } from '../../providers/http-data/http-data'; //Http data service generico
import { MonumentoPage } from '../monumento/monumento';
/**
 * Generated class for the MonumentiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-monumenti',
  templateUrl: 'monumenti.html',
})
export class MonumentiPage {
  typeId: any;
  type: Type;
  monuments: Array<Monument>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private _httpDataService: HttpDataProvider) {
    this.typeId = navParams.get('type');
    this.type = new Type();
    _httpDataService.readAll('monument').then((res) => { this.monuments = res.json(); this.monuments = this.monuments.filter((x) => x.id_type == this.typeId) });
    _httpDataService.readSingle('type', this.typeId).then((res) => { this.type = res.json() });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MonumentiPage');
  }
  itemTapped(event, id) {
    this.navCtrl.push(MonumentoPage, { id: id });
  }
}
export class Monument { nome: string; lon: number; lat: number; id_type: number; type: Type; path: Path; id: number };
export class Type { description: string; namedIcon: string; id: number; icon: string };
export class Path { nome: string; pivot: any; id: number; monument: any };
