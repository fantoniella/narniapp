import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MappePage } from './mappe';

@NgModule({
  declarations: [
    MappePage,
  ],
  imports: [
    IonicPageModule.forChild(MappePage),
  ],
})
export class MappePageModule {}
