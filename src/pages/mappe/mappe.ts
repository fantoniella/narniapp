import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpDataProvider } from '../../providers/http-data/http-data'; //Http data service generico
// import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the MappePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-mappe',
  templateUrl: 'mappe.html',
})
export class MappePage {
  // map: GoogleMap;
  //   myPosmarker: Marker;
  //   whispersMarkers: Array<Marker>;
  //   lat: number;
  //   lon: number;
  // isPlaying: boolean;
  // positionSub: any;
  // fencesTimeout: any;
  // follow: boolean;
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  monuments: Array<Monument>;
  markerOpen: any;
  constructor(public navCtrl: NavController//,
    //   private googleMaps: GoogleMaps,
    //   // private geolocation: Geolocation,
    //   // private device: Device,
    //   private _httpDataService: HttpDataProvider
    //
    , private _httpDataService: HttpDataProvider) {
    _httpDataService.readAll('monument_thumb').then((res) => {
      this.monuments = res.json();
      // this.monuments = this.monuments.filter((x) => x.id_type == this.typeId);
      // console.log(this.monuments);
      this.addMarkers();
      this.markerOpen = false;
    });

  }

  loadMap() {

    // this.geolocation.getCurrentPosition().then((position) => {

    let latLng = new google.maps.LatLng(42.5194, 12.5157); // Narni
    // let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    // }, (err) => {
    //   console.log(err);
    // });

  }
  addMarkers() {
    console.log(this.monuments);

    for (var m of this.monuments) {
      // console.log("Block statement execution no." + i);
      // console.log(this.map.getCenter());
      let latLng = new google.maps.LatLng(m.lat, m.lon); // Narni
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: latLng//this.map.getCenter()
      });
      // this._httpDataService.readSpecial('monument', m.id + '/th').then((res) => {
      //  this._httpDataService.readSpecial('monument', '9/th').then((res) => {
      // console.log(res._body)
      let content = "<div class='mapInfo'><h4>" + m.nome + "</h4><br><img src='" + m.thumbnail + "' size=50%>" +
        "<button (click)=navigate(" + m.id + ");>Naviga</button>" +
        "<button (click)=goTo(" + m.id + ");>Vai</button></div>";
      this.addInfoWindow(marker, content); //.then(()=>{console.log("Marker aggiunto")});
      // });
    }
    console.log("Addedd markers")

  }
  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
      this.markerOpen = true;
    });
    // google.maps.event.addListener(infoWindow, 'closeclick', function() {
    // alert("I'm Closed");
    // });

  }
  goTo(id) { console.log("GoTo " + id); }
  navigate(id) { console.log("Navigate " + id); }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MappePage');
    this.loadMap();

  }
}
export class Monument { nome: string; lon: number; lat: number; id_type: number; type: Type; path: Path; id: number; thumbnail: any };
export class Type { description: string; namedIcon: string; id: number; icon: string };
export class Path { nome: string; pivot: any; id: number };
