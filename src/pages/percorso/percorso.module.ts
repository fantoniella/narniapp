import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PercorsoPage } from './percorso';

@NgModule({
  declarations: [
    PercorsoPage,
  ],
  imports: [
    IonicPageModule.forChild(PercorsoPage),
  ],
})
export class PercorsoPageModule {}
