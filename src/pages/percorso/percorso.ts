import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpDataProvider } from '../../providers/http-data/http-data'; //Http data service generico

/**
 * Generated class for the PercorsoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-percorso',
  templateUrl: 'percorso.html',
})
export class PercorsoPage {
  @ViewChild('map') mapElement: ElementRef;
  pathId: any;
  map: any;
  monuments: Array<Monument>;
  path: Path;
  markerOpen: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private _httpDataService: HttpDataProvider) {
    this.pathId = navParams.get('id');
    this.path = new Path();
    _httpDataService.readSingle('path', this.pathId).then((res) => {
      console.log(this.pathId);
      this.path = res.json();
      this.addMarkers();
      this.markerOpen = false;
    });
  }

  loadMap() {
    console.log("MAppa!")
    // this.geolocation.getCurrentPosition().then((position) => {

    let latLng = new google.maps.LatLng(42.5194, 12.5157); // Narni
    // let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    // }, (err) => {
    //   console.log(err);
    // });

  }
  addMarkers() {
    console.log(this.path.monument);
    if (this.path.monument) {
      for (var m of this.path.monument) {
        let latLng = new google.maps.LatLng(m.lat, m.lon); // Narni
        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: latLng//this.map.getCenter()
        });
        // this._httpDataService.readSpecial('monument', m.id + '/th').then((res) => {
        //  this._httpDataService.readSpecial('monument', '9/th').then((res) => {
        // console.log(res._body)
        let content = "<div class='mapInfo'><h4>" + m.nome + "</h4>" +
          "<button (click)=navigate(" + m.id + ");>Naviga</button>" +
          "<button (click)=goTo(" + m.id + ");>Vai</button></div>";
        this.addInfoWindow(marker, content);
      }
      console.log("Addedd markers")
    }
  }
  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
      this.markerOpen = true;
    });
  }
  goTo(id) { console.log("GoTo " + id); }
  navigate(id) { console.log("Navigate " + id); }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PercorsoPage');
    this.loadMap();
  }
}
export class Monument { nome: string; lon: number; lat: number; id_type: number; type: Type; path: Path; id: number; thumbnail: any };
export class Type { description: string; namedIcon: string; id: number; icon: string };
export class Path { nome: string; pivot: any; id: number; monument: any };
