import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MonumentoPage } from './monumento';

@NgModule({
  declarations: [
    MonumentoPage,
  ],
  imports: [
    IonicPageModule.forChild(MonumentoPage),
  ],
})
export class MonumentoPageModule {}
