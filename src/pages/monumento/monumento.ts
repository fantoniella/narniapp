import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { HttpDataProvider } from '../../providers/http-data/http-data'; //Http data service generico
import { SmartAudio } from '../../providers/smart-audio/smart-audio';


/**
 * Generated class for the MonumentoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-monumento',
  templateUrl: 'monumento.html',
})
export class MonumentoPage {
  monumentoId: any;
  qMonumento: Monument;
  monumento: Monument;
  texts: Text;
  wait: any;
  immagineIngrandita: any;
  immagineGrande: any;
  @ViewChild('audio') audioElement: ElementRef;
  audio: any;
  playing: any;
  tablet: any;
  constructor(public navCtrl: NavController,
            public navParams: NavParams,
            private _httpDataService: HttpDataProvider,
            public smartAudio: SmartAudio,
            public platform: Platform) {
    this.tablet = this.platform.is('tablet');
    this.monumentoId = navParams.get('id');
    this.monumento = new Monument();
    this.qMonumento = new Monument();
    this.immagineIngrandita = false;
    this.playing = false;
    // this.texts = new Text()
    // _httpDataService.readAll('image').then((res)=>{this.images = res.json(); this.images=this.images.filter((x)=>x.id_monument==this.monumentoId)});
    // _httpDataService.readAll('text').then((res)=>{this.texts = res.json(); this.texts=this.images.filter((x)=>x.id_monument==this.monumentoId)});
    this.wait = 1;
    _httpDataService.readSingle('monument', this.monumentoId).then((res) => {
      this.qMonumento = res.json();
      _httpDataService.readSingle('monument_full', this.monumentoId).then((res) => {
        this.monumento = res.json();
        this.texts = this.monumento.text.filter((x) => x.lang == 'it');
        this.wait = 0;
        smartAudio.preload('testo'+this.monumento.nome, this.texts[0].audio);
        // console.log(this.texts[0].audio)
      });
    });


  }
  ingrandisciImmagine(event, url) {
    this.immagineGrande = url;
    this.immagineIngrandita = true;
  };

  riduciImmagine(event) {
    this.immagineIngrandita = false;
  };
  togglePlay(event) {
    if (this.playing) {
      this.playing = false;
      this.smartAudio.stop('testo'+this.monumento.nome)
    }
    else {
      this.playing = true;
      this.smartAudio.play('testo'+this.monumento.nome)
    }
  };

  ionViewDidLoad() {
    console.log('ionViewDidLoad MonumentoPage');
  }
  ionAfterViewInit() {
    // console.log(this.audioElement);
    // this.audio = this.audioElement.nativeElement;

  }
  // ionVie
  ionViewWillLeave() {
    if (this.playing){    this.smartAudio.stop('testo'+this.monumento.nome)}
    this.playing = false;
  }
}

export class Text { id: number; lang: string; id_monument: number; monument: Monument; text: string; audio: string; id_path: number; path: Path; }
export class Image { description: string; largeImage: string; id: number; image: string; monument: Monument; id_path: number; thumbnail: string; id_monument: number }
export class Monument { nome: string; lon: number; lat: number; id_type: number; type: Type; path: Path; id: number; image: any; text: any };
export class Type { description: string; namedIcon: string; id: number; icon: string };
export class Path { nome: string; pivot: any; id: number };
