import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpDataProvider } from '../../providers/http-data/http-data'; //Http data service generico
import { PreferencesProvider } from '../../providers/preferences/preferences';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  monuments: Array<Monument>;
  images: Array<Image>;
  texts: Array<Text>;
  text: Text;
  lang: Array<String>;
  constructor(public navCtrl: NavController,
              private _httpDataService: HttpDataProvider,
              public optionProv: PreferencesProvider ) {
    _httpDataService.readAll('monument').then((res)=>{this.monuments = res.json()});
    _httpDataService.readAll('image').then((res)=>{this.images = res.json()});
    // _httpDataService.readAll('text').then((res)=>{this.texts = res.json()});
    this.text = new Text();
    this.lang = ['it','en'];
    _httpDataService.readSingle('text',12).then((res)=>{this.text = res.json()});
    console.log(optionProv)
  }
}
export class Text {id: number; lang: string; id_monument: number; monument: Monument; text: string; audio: string; id_path: number; path: Path;}
export class Image {description: string; largeImage: string; id: number; image: string; monument: Monument; id_path: number; thumbnail: string; id_monument: number}
export class Monument {nome: string; lon: number; lat: number ; id_type: number; type: Type; path: Path; id: number};
export class Type {description: string; namedIcon: string; id: number; icon: string};
export class Path {nome: string; pivot: any; id: number};
