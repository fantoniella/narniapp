import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpDataProvider } from '../../providers/http-data/http-data'; //Http data service generico
import { PercorsoPage } from '../percorso/percorso';

/**
 * Generated class for the PercorsiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-percorsi',
  templateUrl: 'percorsi.html',
})
export class PercorsiPage {
  paths: Array<Path>;
  constructor(public navCtrl: NavController, public navParams: NavParams, private _httpDataService: HttpDataProvider) {
    _httpDataService.readAll('path').then((res) => { this.paths = res.json();});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PercorsiPage');
  }
  itemTapped(event, id) {
    this.navCtrl.push(PercorsoPage, { id: id });
  }

}

export class Path { nome: string; pivot: any; id: number };
