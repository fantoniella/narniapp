import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { NavController, NavParams } from 'ionic-angular';


import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { MonumentoPage } from '../pages/monumento/monumento';
import { MonumentiPage } from '../pages/monumenti/monumenti';
import { MappePage } from '../pages/mappe/mappe';
import { PercorsiPage } from '../pages/percorsi/percorsi';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  lang: Array<string>;
  pages: Array<{title: string, component: any, icon: any, type: any}>;

  // constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public navCtrl: NavController) {
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
      this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, type:0, icon:'../assets/icon/1.jpg' },
      { title: 'Luoghi di ristoro', component: MonumentiPage, type:4, icon:'../assets/icon/1.jpg' },
      { title: 'Monumenti sacri', component: MonumentiPage, type:1, icon:'../assets/icon/2.jpg' },
      { title: 'Monumenti civili', component: MonumentiPage, type:2, icon:'../assets/icon/3.jpg' },
      { title: 'Panorama&natura', component: MonumentiPage, type:5, icon:'../assets/icon/3.jpg' },
      { title: 'Eventi', component: MonumentiPage, type:6, icon:'../assets/icon/3.jpg' },
      { title: 'Percorsi', component: PercorsiPage, type:0, icon:'../assets/icon/4.jpg' },
      { title: 'Select Language', component: HomePage,type:0, icon:'../assets/icon/5.jpg' },
      { title: 'Mappa', component: MappePage, type:0, icon:'../assets/icon/6.jpg' }
    ];
    // this.lang = ['it','en']

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    // this.nav.setRoot(page.component+"/"+page.type)
    // this.nav.setRoot(page.component,{type: page.type});
        this.nav.push(page.component, {
      type: page.type
    });
;
  }
}
