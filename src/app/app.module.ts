import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { MonumentoPage } from '../pages/monumento/monumento';
import { MonumentiPage } from '../pages/monumenti/monumenti';
import { MappePage } from '../pages/mappe/mappe';
import { PercorsoPage} from '../pages/percorso/percorso';
import { PercorsiPage} from '../pages/percorsi/percorsi';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpDataProvider } from '../providers/http-data/http-data';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpModule, Http } from '@angular/http'
import { SmartAudio } from '../providers/smart-audio/smart-audio';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { PreferencesProvider } from '../providers/preferences/preferences';

// import { Geolocation } from '@ionic-native/geolocation';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    MonumentoPage,
    MonumentiPage,
    MappePage,
    PercorsoPage,
    PercorsiPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    MonumentoPage,
    MonumentiPage,
    MappePage,
    PercorsoPage,
    PercorsiPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // Geolocation,
    SmartAudio,
    NativeAudio,
    HttpDataProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PreferencesProvider
  ]
})
export class AppModule {}
